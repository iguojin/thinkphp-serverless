<?php

namespace app\index\controller;

class Index
{
    public static $sum;

    public function __construct()
    {
        static::$sum++;
    }

    public function index()
    {
        cache('1', 2);

        if (request()->file('binary')) {
            return '文件大小:' . request()->file('binary')->getSize();
        }
        return '<h1>' . date('Y-m-d H:i:s') . '</h1>';
    }

    public function info()
    {
        ob_start();
        phpinfo();
        return $buffer = ob_get_contents();
    }

    public function agent()
    {
        return request()->header('user-agent');
    }

    public function header()
    {
        return json_encode(request()->header(), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
}
